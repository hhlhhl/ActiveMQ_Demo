
package com.hhl.demo1;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * 说明:发布<br/>
 * 创建时间：2018年11月29日 下午9:55:29<br/>
 * @author hhl<br/>
 */
public class Producer2 {

	public static void main(String[] args) throws JMSException {

		// 获取mq连接工程
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
				ActiveMQConnection.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");
		// 创建连接
		Connection createConnection = connectionFactory.createConnection();
		// 启动连接
		createConnection.start();
		// 创建会话工厂
		Session session = createConnection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
		MessageProducer producer = session.createProducer(null);
		// 不持久化
		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		for (int i = 1; i <= 3; i++) {
			System.out.println("我是生产者: " + i);
//			sendMsg(session, producer, "我是生产者: " + i);
			TextMessage textMessage = session.createTextMessage("hello activemq " + i);
			Destination destination = session.createTopic("hhl-topic");
			producer.send(destination,textMessage);

		}
		System.out.println("生产者 发送消息完毕!!!");
		session.close();
		createConnection.close();
	}

	public static void sendMsg(Session session, MessageProducer producer, String i) throws JMSException {
		TextMessage textMessage = session.createTextMessage("hello activemq " + i);
		Destination destination = session.createTopic("hhl-topic");
		producer.send(destination,textMessage);
	}

}
