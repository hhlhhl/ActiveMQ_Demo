package com.hhl.demo;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * 说明:消费者<br/>
 * 创建时间：2018年11月28日 下午10:34:14<br/>
 * @author hhl<br/>
 */
public class Consumer {
	public static void main(String[] args) throws JMSException {
		// 获取mq连接工程
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
				ActiveMQConnection.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");
		// 创建连接
		Connection createConnection = connectionFactory.createConnection();
		// 启动连接
		createConnection.start();
		// 创建会话工厂   createConnection.createSession（是否以事务方式提交，自动签收）
		Session session = createConnection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
		// 创建队列
		Destination destination = session.createQueue("hhl_Queue");
		MessageConsumer createConsumer = session.createConsumer(destination);
		while (true) {
			// 监听消息(超过1秒,则超时)
			TextMessage textMessage = (TextMessage) createConsumer.receive(1000);
			if (textMessage != null) {
				String text = textMessage.getText();
				System.out.println("消费者 获取到消息:  text:" + text);
				
				//手动签收消息 createConnection.createSession(Boolean.FALSE, Session.CLIENT_ACKNOWLEDGE)
				textMessage.acknowledge();
				//提交事务   createConnection.createSession(Boolean.TRUE,Session.AUTO_ACKNOWLEDGE)时，为事务提交，必须要session.commit()
				session.commit();
			} else {
				createConsumer.close();
				break;
			}
		}
		System.out.println("消费者消费消息完毕!!!");
		session.close();
		createConnection.close();
	}
}
